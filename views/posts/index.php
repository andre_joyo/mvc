<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Posts</title>
	<style>
		<?php require 'public/css/default.css'; ?>
	</style>
</head>
<body>
	<?php 
		session_start();
		if (isset($_SESSION['user'])) {
			require 'views/logout.php';
			echo "Usuario: ".$_SESSION['user'].'<br>';
		} else{
			require 'views/login.php';
		}
		var_dump($_SESSION);
	?>
	<h1>Bienvenido a Posts</h1>
	<?php 
		require 'views/nav.php';
		if (isset($_SESSION['user'])) {
			require 'views/admin.php';
		}
	?>
	<div id="posts">
		<a href=<?php echo constant('URL').'posts/getPosts' ?> >Mostrar posts</a>
	</div>
	<div id="comments">
		
	</div>
	<div id="newComment">
		<form id="newCommentForm" method="POST" style="visibility:hidden">
			<input type="text" name="post_id" id="input_post_id" style="visibility:hidden">
			<textarea rows="4" cols="50" name="comment" placeholder="Comentar aquí"></textarea>
			<input type="email" name="email" placeholder="email">
			<input type="submit" value="Enviar comentario">
		</form>
	</div>
</body>
</html>