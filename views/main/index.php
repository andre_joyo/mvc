<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Main</title>
	<style>
		<?php require 'public/css/default.css'; ?>
	</style>
</head>
<body>
	<?php 
		session_start();
		if (isset($_SESSION['user'])) {
			require 'views/logout.php';
			echo "Usuario: ".$_SESSION['user'].'<br>';
			require 'views/admin.php';
		} else{
			require 'views/login.php';
		}
		//var_dump($_SESSION);
	?>
	<h1>Bienvenido al sitio</h1>
	<?php
		require 'views/nav.php'
	?>
	<!--iframe src="https://www.eldiario.es" width="100%" height="700px">
	  <p>Your browser does not support iframes.</p>
	</iframe-->
	<h2>Bienvenido a este blog</h2>
	<p>Puedes acceder a los posts, ir a la sección de categorías o ponerte en contacto con el administrtador a través de la barra de navegación. </p>
	<p>También puedes crear una cuenta e iniciar sesión en este sitio con ella.</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis distinctio atque iusto, quibusdam deleniti sit doloribus eum beatae nesciunt accusantium rerum soluta aliquid suscipit ea ad praesentium animi sequi a!</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis distinctio atque iusto, quibusdam deleniti sit doloribus eum beatae nesciunt accusantium rerum soluta aliquid suscipit ea ad praesentium animi sequi a!</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis distinctio atque iusto, quibusdam deleniti sit doloribus eum beatae nesciunt accusantium rerum soluta aliquid suscipit ea ad praesentium animi sequi a!</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis distinctio atque iusto, quibusdam deleniti sit doloribus eum beatae nesciunt accusantium rerum soluta aliquid suscipit ea ad praesentium animi sequi a!</p>
	
</body>
</html>