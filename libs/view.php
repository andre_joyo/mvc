<?php
	class View{
		function __construct(){
			//echo "<p>Vista base</p>";
		}
		function render($nombre){
			require 'views/'.$nombre.'.php';
		}
		function renderPosts($posts){		
			for ($j=0; $j < count($posts); $j++) { 
					echo "
						<script> 
							element=document.getElementById('posts');
							
							div=document.createElement('div');
							div.className='post';

							title=document.createElement('div');
							title.id='title';
							t_title=document.createTextNode('".$posts[$j]->title."');
							title.appendChild(t_title);
							div.appendChild(title);

							brief=document.createElement('div');
							brief.id='brief';
							t_brief=document.createTextNode('".$posts[$j]->brief."');
							brief.appendChild(t_brief);
							div.appendChild(brief);
							
							t_a=document.createTextNode('Ver post completo');
							link=document.createElement('a');
							link.href='". constant('URL')."posts/getPost/".$posts[$j]->id."' ;
							link.appendChild(t_a);
							div.appendChild(link);

							element.appendChild(div);
						</script>";
			}

		}
		function renderPostsCategory($posts){		
			for ($j=0; $j < count($posts); $j++) { 
					echo "
						<script> 
							element=document.getElementById('posts');
							
							div=document.createElement('div');
							div.className='post';

							title=document.createElement('div');
							title.id='title';
							t_title=document.createTextNode('".$posts[$j]->title."');
							title.appendChild(t_title);
							div.appendChild(title);

							brief=document.createElement('div');
							brief.id='brief';
							t_brief=document.createTextNode('".$posts[$j]->brief."');
							brief.appendChild(t_brief);
							div.appendChild(brief);
							
							t_a=document.createTextNode('Ver post completo');
							link=document.createElement('a');
							link.href='". constant('URL')."posts/getPost/".$posts[$j]->id."' ;
							link.appendChild(t_a);
							div.appendChild(link);

							element.appendChild(div);
						</script>";
			}

		}
		function renderPost($post, $comments){	
		//var_dump($comments);
			echo "
				<script> 
					element=document.getElementById('posts');

					comments_title=document.createElement('h2');
					t_comments_title=document.createTextNode('Comentarios');
					comments_title.appendChild(t_comments_title);
					comments.appendChild(comments_title);

					title=document.createElement('div');
					title.id='title';
					t_title=document.createTextNode('".$post[0]->title."');
					title.appendChild(t_title);
					element.appendChild(title);

					content=document.createElement('div');
					content.id='content';
					t_content=document.createTextNode('".$post[0]->content."');
					content.appendChild(t_content);
					element.appendChild(content);
					
				</script>";
			for ($j=0; $j < count($comments); $j++) { 
			echo "<script>";
				echo "comments=document.getElementById('comments');";
				echo "comment=document.createElement('div');";
				echo "comment.className='comment';";
				
				echo "p_comment=document.createElement('p');";
				echo "t_p_comment=document.createTextNode('".$comments[$j]->comment."');";
				echo "p_comment.appendChild(t_p_comment);";
				echo "comment.appendChild(p_comment);";

				echo "comments.appendChild(comment);";
			echo "</script>";
			}
			echo "	<script>
						formComment=document.getElementById('newCommentForm');
						formComment.style.visibility='visible';
						formComment.action='".constant('URL')."posts/addComment/".$post[0]->id."';

						inputHidden=document.getElementById('input_post_id');
						inputHidden.value=".$post[0]->id.";
					</script>";

		}
		function renderPostsAdmin($posts){		
			for ($j=0; $j < count($posts); $j++) { 
					echo "
						<script> 
							posts=[]; 
							posts=document.getElementById('posts');
							posts[$j]=[]; 
							//t = document.createTextNode('".$posts[$j]->title." ".$posts[$j]->brief." ".$posts[$j]->status."');
							div=document.createElement('div'); 
							//div.appendChild(t);

							t_a=document.createTextNode('Editar post '+".$posts[$j]->id.");
							link=document.createElement('a');
							link.href='". constant('URL')."admin/editPost/".$posts[$j]->id."' ;
							link.appendChild(t_a);
							div.appendChild(link);

							title=document.createElement('h1');
							t_tile=document.createTextNode('".$posts[$j]->title."');
							title.appendChild(t_tile);
							div.appendChild(title);

							brief=document.createElement('p');
							t_brief=document.createTextNode('".$posts[$j]->brief."');
							brief.appendChild(t_brief);
							div.appendChild(brief);

							posts.appendChild(div);
						</script>";
			}

		}
		function renderPostAdmin($post, $comments){
			for ($j=0; $j < count($post); $j++) { 
				echo "
					<script> 
						post=[]; 
						posts=document.getElementById('posts');
						post[$j]=[]; 
						t2 = document.createTextNode('".$post[$j]->title." ".$post[$j]->brief." ".$post[$j]->id." ".$post[$j]->status." ".$post[$j]->content."');
						t_content='".$post[$j]->content."';
						t_status='".$post[$j]->status."';

						form=document.createElement('form'); 
						form.id='form';
						form.method='POST';
						form.action= '". constant('URL')."admin/updatePost/".$post[$j]->id."';
						
						title=document.createElement('input');
						title.type = 'text';
						title.name='title';
						t_tile='".$post[$j]->title."';
						title.value=t_tile;
						form.appendChild(title);

						brief=document.createElement('input');
						brief.type = 'text';
						brief.name='brief';
						t_brief='".$post[$j]->brief."';
						brief.value=t_brief;
						form.appendChild(brief);

						content=document.createElement('textarea');
						content.rows = '7';
						content.cols = '70';
						content.name='content';
						t_brief='".$post[$j]->content."';
						content.value=t_content;
						form.appendChild(content);

						status_post=document.createElement('input');
						status_post.type = 'text';
						status_post.name='status_post';
						t_status_post='".$post[$j]->status."';
						status_post.value=t_status_post;
						form.appendChild(status_post);


						form.appendChild(t2);

						//
						
						submit=document.createElement('input');
						submit.type='submit';
						submit.value='Actualizar post';
						form.appendChild(submit);

						posts.appendChild(form);
					</script>";
				echo "
					<script> 
						posts=document.getElementById('posts');

						formDelete=document.createElement('form'); 
						formDelete.id='formDelete';
						formDelete.method='POST';
						formDelete.action= '". constant('URL')."admin/deletePost/".$post[$j]->id."';
						
						submit=document.createElement('input');
						submit.type='submit';
						submit.value='Eliminar post';
						formDelete.appendChild(submit);

						posts.appendChild(formDelete);
					</script>";
			}
			for ($j=0; $j < count($comments); $j++) { 
				echo "<script>";
					echo "comments=document.getElementById('comments');";
					echo "comment=document.createElement('form');";
					echo "comment.className='comment';";
					
					echo "p_comment=document.createElement('p');";
					echo "t_p_comment=document.createTextNode('".$comments[$j]->comment."');";
					echo "p_comment.appendChild(t_p_comment);";
					echo "comment.appendChild(p_comment);";

					echo "delete_button=document.createElement('')";

					echo "comments.appendChild(comment);";
				echo "</script>";
			}
		}
		function renderCategories($categories){		
			for ($j=0; $j < count($categories); $j++) { 
					echo "
						<script> 
							categories=[]; 
							categories=document.getElementById('categories');
							categories[$j]=[]; 
							t = document.createTextNode('".$categories[$j]->name." ');
							div=document.createElement('div'); 
							div.appendChild(t);

							t_a=document.createTextNode(".$categories[$j]->id.");
							link=document.createElement('a');
							link.href='". constant('URL')."categories/displayPostsCategory/".$categories[$j]->id."' ;
							link.appendChild(t_a);
							div.appendChild(link);

							categories.appendChild(div);
						</script>";
			}	
		}
		function renderCategoriesAdmin($categories){		
			for ($j=0; $j < count($categories); $j++) { 
					echo "
						<script> 
							categories=[]; 
							categories=document.getElementById('categories');
							categories[$j]=[]; 
							t = document.createTextNode('".$categories[$j]->name." ');
							div=document.createElement('div'); 
							div.appendChild(t);

							t_a=document.createTextNode(".$categories[$j]->id.");
							link=document.createElement('a');
							link.href='". constant('URL')."admin/editCategory/".$categories[$j]->id."' ;
							link.appendChild(t_a);
							div.appendChild(link);

							categories.appendChild(div);
						</script>";
			}	
		}

		function renderCategoryAdmin($category){
			for ($j=0; $j < count($category); $j++) { 
				echo "
					<script>
						category=[];
						categories=document.getElementById('categories');
						category[$j]=[];
						t2 = document.createTextNode('".$category[$j]->id." ".$category[$j]->name."');

						form=document.createElement('form');
						form.id='form';
						form.method='POST';
						form.action= '". constant('URL')."admin/updateCategory/".$category[$j]->id."';
						
						nameCat=document.createElement('input');
						nameCat.type = 'text';
						nameCat.name='nameCat';
						t_tile='".$category[$j]->name."';
						nameCat.value=t_tile;
						form.appendChild(nameCat);

						form.appendChild(t2);

						//
						
						submit=document.createElement('input');
						submit.type='submit';
						submit.value='Actualizar categoría';
						form.appendChild(submit);

						categories.appendChild(form);
					</script>";
				echo "
					<script> 
						posts=document.getElementById('posts');

						formDelete=document.createElement('form'); 
						formDelete.id='formDelete';
						formDelete.method='POST';
						formDelete.action= '". constant('URL')."admin/deleteCategory/".$category[$j]->id."';						
						
						submit=document.createElement('input');
						submit.type='submit';
						submit.value='Eliminar categoría';
						formDelete.appendChild(submit);

						posts.appendChild(formDelete);
					</script>";
			}
		}
		function renderOptionsCategories($categories){		
			for ($j=0; $j < count($categories); $j++) { 
				echo "
					<script> 
						category=[]; 
						category=document.getElementById('category');
						categories[$j]=[]; 
						t = document.createTextNode('".$categories[$j]->name." ');
						option=document.createElement('option'); 
						option.appendChild(t);
						category.appendChild(option);
					</script>";
			}
		}
	}
?>