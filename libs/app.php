<?php
	class App{
		function __construct(){
			//echo "<p>Nueva app</p>";
			$url = $_GET['url'];
			$url = rtrim($url, '/');
			$url = explode('/', $url);
			//var_dump($url);
			require 'controllers/errores.php';
			if (empty($url[0]) || $url[0]=='index.php') {
				require_once 'controllers/main.php';
				$controller=new Main();
				return false;
			}
			$archivo_controller = 'controllers/'.$url[0].'.php';
			if (file_exists($archivo_controller)) {
				require_once $archivo_controller;
				$controller = new $url[0];
				$controller->loadModel($url[0]);
				if ($url[0]=='admin') {
					$controller->displayCategories();
					if (!empty($url[1])) {
						switch ($url[1]) {
						    case 'editPost':
						        $controller->{$url[1]}($url[2]);
						        break;
						    case 'updatePost':
						        $title=$_POST['title'];
								$brief=$_POST['brief'];
								$content=$_POST['content'];
								$controller->{$url[1]}($url[2],$title,$brief,$content);
						        break;
						    case 'deletePost':
								$controller->{$url[1]}($url[2]);
						        break;
						    case 'editCategory':
								$controller->{$url[1]}($url[2]);
						        break;
						    case 'updateCategory':
								$name=$_POST['nameCat'];
								$controller->{$url[1]}($url[2], $name);
						        break;
						    case 'deleteCategory':
						        $controller->{$url[1]}($url[2]);
						        break;
						    case 'addPost':
						        $title=$_POST['title'];
								$brief=$_POST['brief'];
								$content=$_POST['content'];
								$controller->{$url[1]}($title,$brief,$content);
						    	break;
						    case 'goOut':
								$controller->{$url[1]}();
						    	break;
						    case 'addCategory':
								$controller->{$url[1]}();
						    	break;
						    case 'getCategoriesAdmin':
								$controller->{$url[1]}();
						    	break;
						    case 'getPostsAdmin':
								$controller->{$url[1]}();
						    	break;
						  	default:
       						echo "Método admin no encontrado";
						}
					}					
				} elseif (!empty($url[1])) {
					switch ($url[1]) {
					    case 'getPosts':
							$controller->getPosts();
					        break;					
					    case 'getPost':
							if (isset($url[2])) {
								$controller->{$url[1]}($url[2]);
							} else{
								$controller->getPosts();
							}
					        break;    
					    case 'access':
					        $user=$_POST['userName'];
							$password=$_POST['password'];
							$controller->{$url[1]}($user,$password);
					    	break;
				    	case 'goOut':
							$controller->{$url[1]}();
					    	break;
						case 'getCategories':
							$controller->{$url[1]}();
					    	break;
					    case 'displayPostsCategory':					    	
							$controller->{$url[1]}($url[2]);
					    	break;
					    default:
       						echo "Método no encontrado";
					}
				}
			} else{
				$controller = new Errores;
			}
		}
	}
?>