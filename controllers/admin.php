<?php
	class Admin extends Controller{
		function __construct(){
			parent::__construct();
			$this->view->render('admin/index');
			//echo "<p>Nuevo controlador admin</p>";
		}
		function displayCategories(){
			$categories=$this->model->getCategories();
			$this->view->renderOptionsCategories($categories);			

		}
		function addPost(){
			echo "stringstringstringstringstringstringstring";
			sleep(2);
			$title=$_POST['title'];
			$brief=$_POST['brief'];
			$content=$_POST['content'];
			$category_id=intval($_POST['category']);			
			$user_id=1;
			$status=$_POST['statusPost'];
			$insertion=$this->model->addPost(['title'=>$title, 'brief'=>$brief, 'content'=>$content, 'category_id'=>$category_id, 'user_id'=>$user_id, 'status'=>$status]);
			if ($insertion) {
				$message='Inserción correcta';
			} else{
				$message='Inserción no completada';
			}
		}
		function getPostsAdmin(){
			$posts=$this->model->getPosts();
			$this->view->renderPostsAdmin($posts);
			//var_dump($posts);
		}
		function getCategoriesAdmin(){
			$categories=$this->model->getCategories();
			$this->view->renderCategoriesAdmin($categories);
			//var_dump($categories);
		}
		function editPost($id){
			$post=$this->model->getPost($id);
			$comments=$this->model->getComments($id);
			$this->view->renderPostAdmin($post, $comments);
			echo "EDITING POST<br>";
			//var_dump($post);
		}
		function editCategory($id){
			$category=$this->model->getCategory($id);
			$this->view->renderCategoryAdmin($category);
			echo "EDITING CATEGORY<br>";
			//var_dump($category);
		}
		function updatePost($id,$title,$brief,$content){
			echo "POST para actualizar<br>";
			//var_dump($_POST);
			$post=$this->model->updatePost($id,$title,$brief,$content);
			//$this->view->renderPostsAdmin($post);
			echo "POST actualizado<br>";
			//var_dump($post);
		}
		function updateCategory($id,$name){
			echo "POST para actualizar<br>";
			//var_dump($_POST);
			$category=$this->model->updateCategory($id,$name);
			//$this->view->renderCategoriesAdmin($category);
			echo "CATEGORÍA actualizada<br>";
			//var_dump($category);
		}
		function deletePost($id){
			echo "POST para borrar<br>";
			//var_dump($_POST);
			$post=$this->model->deletePost($id);
			//$this->view->renderPostsAdmin($post);
			echo "POST eliminado<br>";
			//var_dump($post);
		}
		function deleteCategory($id){
			echo "CATEGORÍA para borrar<br>";
			//var_dump($_POST);
			$category=$this->model->deleteCategory($id);
			//$this->view->renderCategoriesAdmin($category);
			echo "CATEGORÍA eliminada<br>";
			//var_dump($category);
		}
	}
?>