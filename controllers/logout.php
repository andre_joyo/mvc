<?php
	class Logout extends Controller{
		function __construct(){
			
		}
		function goOut(){
			session_start();
			session_destroy();
			header('location: ../');
		}
	}
?>