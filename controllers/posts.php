<?php
	class Posts extends Controller{
		function __construct(){
			parent::__construct();
			$this->view->render('posts/index');
			//echo "<p>Nuevo controlador posts</p>";
		}
		function addPost(){
			$title=$_POST['title'];
			$brief=$_POST['brief'];
			$content=$_POST['content'];
			$category_id=intval($_POST['category']);			
			$user_id=1;
			$status=$_POST['statusPost'];
			$insertion=$this->model->add(['title'=>$title, 'brief'=>$brief, 'content'=>$content, 'category_id'=>$category_id, 'user_id'=>$user_id, 'status'=>$status]);
			if ($insertion) {
				$message='Inserción correcta';
			} else{
				$message='Inserción no completada';
			}
		}
		function getPosts(){
			$posts=$this->model->get();
			$this->view->renderPosts($posts);
			//var_dump($posts);
		}
		function getPost($id){
			$post=$this->model->getPost($id);
			$comments=$this->model->getComments($id);
			$this->view->renderPost($post, $comments);
			//var_dump($posts);
		}
		function addComment(){
			var_dump($_POST);
			$comment=$_POST['comment'];
			$email=$_POST['email'];
			$post_id=$_POST['post_id'];
			$insertion=$this->model->addComment(['comment'=>$comment, 'email'=>$email, 'post_id'=>$post_id]);
			if ($insertion) {
				$message='Inserción correcta';
			} else{
				$message='Inserción no completada';
			}
		}
	}
?>