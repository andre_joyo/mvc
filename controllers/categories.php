<?php
	class Categories extends Controller{
		function __construct(){
			parent::__construct();
			$this->view->render('categories/index');
			//echo "<p>Nuevo controlador categories</p>";
		}
		function getCategories(){
			$categories=$this->model->getCategories();
			$this->view->renderCategories($categories);
			//var_dump($categories);
		}
		function addCategory(){
			$category=$_POST['category'];
			$insertion=$this->model->add(['category'=>$category]);
			if ($insertion) {
				$message='Inserción correcta';
			} else{
				$message='Inserción no completada';
			}
		}
		function displayPostsCategory($category){
			$posts=$this->model->selectPostsCategory($category);
			$this->view->renderPostsCategory($posts);
		}
	}
?>