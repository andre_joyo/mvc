<?php
	include_once 'models/post.php';
	include_once 'models/category.php';
	class categoriesModel extends Model{
		public function __construct(){
			parent::__construct();
		}
		public function getCategories(){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM category');
				while ($row=$query->fetch()) {
					$item=new Category();
					$item->id=$row['id'];
					$item->name=$row['name'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function add($post){//echo "insertar datos";
			try{
				$query=$this->db->connect()->prepare('INSERT INTO category(name) VALUES (:category)');
				$query->execute(['category'=>$post['category']]);
				echo "Categoría agregada";
				return true;
			} catch(PDOException $e){print_r($e->getMessage());
				echo "Error en la inserción";
				return false;
			}
		}
		public function selectPostsCategory($category_id){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM post WHERE category_id="'.$category_id.'"');
				while ($row=$query->fetch()) {
					$item=new Post();
					$item->title=$row['title'];
					$item->brief=$row['brief'];
					$item->content=$row['content'];
					$item->status=$row['status'];
					$item->brief=$row['brief'];

					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
	}
?>