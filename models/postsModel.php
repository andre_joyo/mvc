<?php
	include_once 'models/post.php';
	include_once 'models/comment.php';
	class postsModel extends Model{
		public function __construct(){
			parent::__construct();
		}
		public function add($post){//echo "insertar datos";
			try{
				$query=$this->db->connect()->prepare('INSERT INTO post(title, brief, content, user_id, category_id, status) VALUES (:title, :brief, :content, :user_id, :category_id, :status)');
				$query->execute(['title'=>$post['title'],'brief'=>$post['brief'],'content'=>$post['content'], 'user_id'=>$post['user_id'], 'category_id'=>$post['category_id'], 'status'=>$post['status']]);
				echo "Post insertado";
				return true;
			} catch(PDOException $e){print_r($e->getMessage());
				echo "Error en la inserción";
				return false;
			}
		}
		public function addComment($comment){//echo "insertar datos";
			try{
				$query=$this->db->connect()->prepare('INSERT INTO comment(comment, email, post_id) VALUES (:comment, :email, :post_id)');
				$query->execute(['comment'=>$comment['comment'],'email'=>$comment['email'],'post_id'=>$comment['post_id']]);
				echo "Comentario insertado";
				return true;
			} catch(PDOException $e){print_r($e->getMessage());
				echo "Error en la inserción";
				return false;
			}
		}
		public function get(){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM post');
				while ($row=$query->fetch()) {
					$item=new Post();
					$item->title=$row['title'];
					$item->brief=$row['brief'];
					$item->content=$row['content'];
					$item->status=$row['status'];
					$item->id=$row['id'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function getPost($id){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM post WHERE id="'.$id.'"');
				while ($row=$query->fetch()) {
					$item=new Post();
					$item->title=$row['title'];
					$item->brief=$row['brief'];
					$item->content=$row['content'];
					$item->status=$row['status'];
					$item->id=$row['id'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
		public function getComments($id){//echo "insertar datos";
			$items=[];
			try{
				$query=$this->db->connect()->query('SELECT * FROM comment WHERE post_id="'.$id.'"');
				while ($row=$query->fetch()) {
					$item=new Comment();
					$item->id=$row['id'];
					$item->comment=$row['comment'];
					$item->created_at=$row['created_at'];
					array_push($items, $item);
					//echo "item: ".$row['title']."<br>";
				}
				//var_dump($items);
				return $items;
			} catch(PDOException $e){
				print_r($e->getMessage());
			}
		}
	}
?>